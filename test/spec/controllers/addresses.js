'use strict';

describe('Controller: AddressesCtrl', function () {

  // load the controller's module
  beforeEach(module('customerApp'));

  var AddressesCtrl,
    scope,
    $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_) {
    $httpBackend = _$httpBackend_;
    scope = $rootScope.$new();

    $httpBackend.when('GET', '/data/addresses.json')
      .respond([{
        "id": 1,
        "name": "Test 1"
      },{
        "id": 2,
        "name": "Test 2"
      }]);

    $httpBackend.when('GET', '/data/customer.json')
      .respond({
        "type": 1,
        "firstName": "Mock",
        "lastName": "test",
        "email": "mocko@test.com",
        "taxId": "11111111111"
      });

    AddressesCtrl = $controller('AddressesCtrl', {
      $scope: scope
    });
  }));

  it('should be data defined', function () {
    $httpBackend.flush();

    expect(scope.customer).toBeDefined();
    expect(scope.addresses).toBeDefined();
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.verifyNoOutstandingExpectation();
  });
});
