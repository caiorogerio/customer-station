'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('customerApp'));

  var MainCtrl,
    scope,
    $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_) {
    $httpBackend = _$httpBackend_;
    scope = $rootScope.$new();

    $httpBackend.expect('GET', '/data/customer.json')
      .respond({
        "type": 1,
        "firstName": "Mock",
        "lastName": "test",
        "email": "mocko@test.com",
        "taxId": "11111111111"
      });

    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should be data defined', function () {
    $httpBackend.flush();

    expect(scope.customer).toBeDefined();
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.verifyNoOutstandingExpectation();
  });
});
