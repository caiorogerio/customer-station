'use strict';

describe('Controller: OrdersCtrl', function () {

  // load the controller's module
  beforeEach(module('customerApp'));

  var OrdersCtrl,
    scope,
    $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_) {
    $httpBackend = _$httpBackend_;
    scope = $rootScope.$new();

    $httpBackend.when('GET', '/data/orders.json')
      .respond([{
        id: 1,
        price: 101.1,
        products: [{
          sku: 1001
        },{
          sku: 1002
        },{
          sku: 1003
        },{
          sku: 1004
        },{
          sku: 1005
        }]
      },{
        id: 2,
        price: 102.2,
        products: [{
          sku: 2001
        },{
          sku: 2002
        }]
      }]);

    $httpBackend.when('GET', '/data/customer.json')
      .respond({
        "type": 1,
        "firstName": "Mock",
        "lastName": "test",
        "email": "mocko@test.com",
        "taxId": "11111111111"
      });

    OrdersCtrl = $controller('OrdersCtrl', {
      $scope: scope
    });
  }));

  it('should be data defined', function () {
    $httpBackend.flush();

    expect(scope.customer).toBeDefined();
    expect(scope.orders).toBeDefined();
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.verifyNoOutstandingExpectation();
  });
});
