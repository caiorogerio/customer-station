'use strict';

describe('Service: Addresses', function () {

  // load the service's module
  beforeEach(module('customerApp'));

  // instantiate service
  var Addresses,
      $httpBackend,
      scope;

  beforeEach(inject(function (_Addresses_, _$httpBackend_, _$rootScope_) {
    Addresses = _Addresses_;
    $httpBackend = _$httpBackend_;
    scope = _$rootScope_.$new();

    $httpBackend.expect('GET', '/data/addresses.json')
      .respond([{
        "id": 1,
        "name": "Test 1"
      },{
        "id": 2,
        "name": "Test 2"
      }]);
  }));

  it('checks address data', function () {
    expect(!!Addresses).toBe(true);

    Addresses(scope);
    $httpBackend.flush();

    expect(scope.addresses).toBeDefined();
    expect(scope.addresses.length).toBe(2);

    expect(scope.addresses[0].id).toBe(1);
    expect(scope.addresses[0].name).toBe('Test 1');

    expect(scope.addresses[1].id).toBe(2);
    expect(scope.addresses[1].name).toBe('Test 2');
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.verifyNoOutstandingExpectation();
  });

});
