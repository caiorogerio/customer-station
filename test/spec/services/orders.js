'use strict';

describe('Service: Orders', function () {

  // load the service's module
  beforeEach(module('customerApp'));

  // instantiate service
  var Orders,
    $httpBackend,
    scope;

  beforeEach(inject(function (_Orders_, $rootScope, _$httpBackend_) {
    Orders = _Orders_;
    $httpBackend = _$httpBackend_;
    scope = $rootScope.$new();

    $httpBackend.expect('GET', '/data/orders.json')
      .respond([{
        id: 1,
        price: 101.1,
        products: [{
          sku: 1001
        },{
          sku: 1002
        },{
          sku: 1003
        },{
          sku: 1004
        },{
          sku: 1005
        }]
      },{
        id: 2,
        price: 102.2,
        products: [{
          sku: 2001
        },{
          sku: 2002
        }]
      }]);
  }));

  it('checks orders data', function () {
    expect(!!Orders).toBe(true);

    Orders(scope);
    $httpBackend.flush();

    expect(scope.orders.length).toBe(2);

    expect(scope.orders[0].id).toBe(1);
    expect(scope.orders[0].price).toBe(101.1);
    expect(scope.orders[0].products.length).toBe(5);

    expect(scope.orders[0].products[0].sku).toBe(1001);
    expect(scope.orders[0].products[1].sku).toBe(1002);
    expect(scope.orders[0].products[2].sku).toBe(1003);
    expect(scope.orders[0].products[3].sku).toBe(1004);
    expect(scope.orders[0].products[4].sku).toBe(1005);

    expect(scope.orders[1].id).toBe(2);
    expect(scope.orders[1].price).toBe(102.2);
    expect(scope.orders[1].products.length).toBe(2);

    expect(scope.orders[1].products[0].sku).toBe(2001);
    expect(scope.orders[1].products[1].sku).toBe(2002);
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.verifyNoOutstandingExpectation();
  });

});
