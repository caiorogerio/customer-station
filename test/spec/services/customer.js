'use strict';

describe('Service: Customer', function () {

  // load the service's module
  beforeEach(module('customerApp'));

  // instantiate service
  var Customer,
    $httpBackend,
    scope,
    expected = {
      "type": 1,
      "firstName": "Mock",
      "lastName": "test",
      "email": "mocko@test.com",
      "taxId": "11111111111"
    };

  beforeEach(inject(function (_Customer_, _$httpBackend_, _$rootScope_) {
    Customer = _Customer_;
    $httpBackend = _$httpBackend_;
    scope = _$rootScope_.$new();

    $httpBackend.expect('GET', '/data/customer.json')
      .respond(expected);
  }));

  it('checks customer data', function () {
    expect(!!Customer).toBe(true);

    Customer(scope);
    $httpBackend.flush();

    expect(scope.customer).toBeDefined();
    angular.forEach(expected, function(value, key) {
      expect(scope.customer[key]).toBe(value);
    });
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.verifyNoOutstandingExpectation();
  });
});
