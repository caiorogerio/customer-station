'use strict';

describe('Service: CustomerDataLayer', function () {

  // load the service's module
  beforeEach(module('customerApp'));

  // instantiate service
  var CustomerDataLayer,
      $httpBackend,
      scope;

  beforeEach(inject(function (_CustomerDataLayer_, _$httpBackend_, _$rootScope_) {
    CustomerDataLayer = _CustomerDataLayer_;
    $httpBackend = _$httpBackend_;
    scope = _$rootScope_.$new();

    $httpBackend.expect('GET', '/data/test.json')
      .respond({
        success: true
      });

  }));

  it('should define scope for data', function () {
    expect(!!CustomerDataLayer).toBe(true);

    CustomerDataLayer('test')(scope);
    $httpBackend.flush();

    expect(scope.test).toBeDefined();
    expect(scope.test.success).toBe(true);
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.verifyNoOutstandingExpectation();
  });

});
