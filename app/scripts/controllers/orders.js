'use strict';

/**
 * @ngdoc function
 * @name customerApp.controller:OrdersCtrl
 * @description
 * # OrdersCtrl
 * Controller of the customerApp
 */
angular.module('customerApp')
  .controller('OrdersCtrl', function ($scope, $rootScope, $routeParams, Customer, Orders) {
    $rootScope.section = 'orders';

    Customer($scope);
    Orders($scope);

    $scope.currentOrder = $routeParams.order;
  });
