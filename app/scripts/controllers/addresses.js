'use strict';

/**
 * @ngdoc function
 * @name customerApp.controller:AddressesCtrl
 * @description
 * # AddressesCtrl
 * Controller of the customerApp
 */
angular.module('customerApp')
  .controller('AddressesCtrl', function ($scope, $rootScope, $routeParams, Customer, Addresses) {
    $rootScope.section = 'addresses';

    Customer($scope);
    Addresses($scope);

    $scope.currentAddress = $routeParams.address;
  });
