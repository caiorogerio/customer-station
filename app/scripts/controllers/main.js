'use strict';

/**
 * @ngdoc function
 * @name customerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the customerApp
 */
angular.module('customerApp')
  .controller('MainCtrl', function ($scope, $rootScope, Customer) {
    $rootScope.section = 'profile';

    Customer($scope);
  });
