'use strict';

/**
 * @ngdoc overview
 * @name customerApp
 * @description
 * # customerApp
 *
 * Main module of the application.
 */
angular
  .module('customerApp', [
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/pedidos', {
        templateUrl: 'views/orders.html',
        controller: 'OrdersCtrl'
      })
      .when('/pedidos/:order', {
        templateUrl: 'views/orders.html',
        controller: 'OrdersCtrl'
      })
      .when('/enderecos', {
        templateUrl: 'views/addresses.html',
        controller: 'AddressesCtrl'
      })
      .when('/enderecos/:address', {
        templateUrl: 'views/addresses.html',
        controller: 'AddressesCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
