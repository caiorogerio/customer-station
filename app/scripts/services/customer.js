'use strict';

/**
 * @ngdoc service
 * @name customerApp.Customer
 * @description
 * # Customer
 * Service in the customerApp.
 */
angular.module('customerApp')
  .service('Customer', function (CustomerDataLayer) {
    return CustomerDataLayer('customer');
  });
