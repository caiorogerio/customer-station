'use strict';

/**
 * @ngdoc service
 * @name customerApp.Addresses
 * @description
 * # Addresses
 * Service in the customerApp.
 */
angular.module('customerApp')
  .service('Addresses', function (CustomerDataLayer) {
    return CustomerDataLayer('addresses');
  });
