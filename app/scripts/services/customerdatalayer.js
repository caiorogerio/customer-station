'use strict';

/**
 * @ngdoc service
 * @name customerApp.CustomerDataLayer
 * @description
 * # CustomerDataLayer
 * Service in the customerApp.
 */
angular.module('customerApp')
  .service('CustomerDataLayer', function ($http) {
    var cache = {};

    return function(dataType) {
      return function(scope, callback) {
        callback = callback || function(){};

        if(!cache[dataType]) {
          $http.get('/data/' + dataType + '.json')
            .success(function(response) {
              cache[dataType] = response;
              scope[dataType] = response;
              callback(response);
            });
        } else {
          scope[dataType] = cache[dataType];
          callback(cache[dataType]);
        }
      };
    };
  });
