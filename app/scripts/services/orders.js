'use strict';

/**
 * @ngdoc service
 * @name customerApp.Orders
 * @description
 * # Orders
 * Service in the customerApp.
 */
angular.module('customerApp')
  .service('Orders', function (CustomerDataLayer) {
    return CustomerDataLayer('orders');
  });
