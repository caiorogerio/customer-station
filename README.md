# Customer Station #
This is a draft front-end for a customer area for an e-commerce using AngularJS + Bootstrap. This project uses Yeoman Angular generator for scaffolding.

###Deps###
AngularJS - Used in order to deal with data in a organised way (MVC) and deal with routes navigation between pages 
Bootstrap - Used for web components, grid, forms, navigation (fixed top and tabs) in a standardised way
Grunt - Task runner for final build (concating, code validator), develop (with livereload, dev server, compile in realtime sass) and run tests
Yeoman - Scaffolding for generate a standard project in an organised directories for views, controllers, tests etc.
Bower - Util for solving dependencies for client libs
SASS - Compiler for stylesheets which helps dealing with bootstrap's variables
jQuery - Lib for deal with DOM elements (better than native one or default angular.element) which is a dependency for Bootstrap
Karma + ngMock - Lib for unit tests and mock for angular

###How to install###
git clone https://caiorogerio@bitbucket.org/caiorogerio/customer-station.git
npm install
bower install
grunt build

The content at dist directory must to be served by some http web server.

###How to develop/preview###
grunt serve
open your browser at http://localhost:9000/

###For unit tests###
The unit tests uses karma + ngMock and could be runned by
grunt test